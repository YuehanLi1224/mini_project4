FROM rust:1.75 AS builder

WORKDIR /usr/src/my_actix_web_app

COPY . .

RUN cargo build --release

EXPOSE 8080

CMD cargo run




